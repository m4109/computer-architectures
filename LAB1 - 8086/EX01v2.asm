MINDIM EQU 2
MAXDIM EQU 50

.MODEL SMALL
.STACK
.DATA   
first_line  DB 50 DUP(?)
second_line DB 50 DUP(?)
third_line  DB 50 DUP(?)
fourth_line DB 50 DUP(?)   
countline   DB 52 DUP(0)   
MAX DB ?  
MAXWORD DB ?                
linefeed db 13, 10, "$"

.CODE   
.STARTUP    
            MOV DI, 0

read1:      MOV AH, 1              ; Read and store
            INT 21H                ; the character
            MOV first_line[DI], AL ; in memory.
            
            INC DI                 ; update of index
            
            CMP DI, MINDIM         ; if DI <= BX
            JBE noCheck            ; jump the /n check
            
            CMP AL, 13             ; \n check
            JZ exit1 

noCheck:    CMP DI, MAXDIM         ; looping
            JNZ read1  
            
            ; new line
            MOV AH, 09
            MOV DX, offset linefeed
            INT 21h
            
exit1:  ;   OCCURENCIES COMPUTING
            MOV CX, DI             ; CX = string length     
            MOV DI, 0h 
            XOR BH, BH
occLoop:    MOV BL, first_line[DI] 
            INC DI
            CMP BL, 64
            JBE occLoop
            
            CMP BL, 90
            JBE maiusc 
            
            CMP BL, 96
            JBE occLoop
            
            CMP BL, 122
            JBE minusc
            
maiusc:     INC countline[BX - 65]
            JMP occLoopReturn

minusc:     INC countline[BX - 71]
         
occLoopReturn:
            CMP DI, CX
            JLE occLoop
            ; find the maximum 
            MOV DI, 0  
            MOV DX, 0 ; index of maximum
            MOV AX, 0 ; local maximum
maxLoop:    MOV BL, countline[DI]
            
            CMP BL, AL
            JBE noMax
            ; new Max found
            
            MOV AX, BX
            MOV DX, DI
            
noMax:      INC DI
            
            CMP DI, 52
            JB maxLoop 
                      
            MOV MAX, AL            ; update in memory MAX VALUE    
            
            CMP DX, 25
            JBE maiuscMax
     
            ADD DX, 71
            MOV MAXWORD, DL
            JMP endif
            
maiuscMax:  ADD DX, 65
            mov MAXWORD, DL
            
endif:      MOV AH, 02H
            MOV AL, MAXWORD
            INT 21H
            
            MOV AL, 20H
            INT 21H     
            ; new line
            MOV AH, 09
            MOV DX, offset linefeed
            INT 21h
             
            ; max/2
            XOR BH, BH
            MOV BL, MAX
            SAR AX, 1 ; compute AX/2
            
            XOR DI, DI; DI = 0
max2Loop:   MOV DL, countline[DI]   
            
            CMP BL, AL
            JB noPrintMax
              
              
         
noPrintMax: INC DI
            CMP DI, CX
            JB max2Loop           
             
             
             
             
            ; printing
            MOV DI, 0              ; printing loop       
print1:     MOV DL, first_line[DI] ; DL <- ACTUAL CHARACTER TO PRINT
            
            CMP DL, 13             ; check if character is /n, don't print
            JZ endprint1
            
            MOV AH, 02H
            INT 21H
            
            INC DI
            
            CMP DI, CX
            JNZ print1   
endprint1:           
.EXIT
END

