.model SMALL
.STACK
 
.DATA  
N EQU 2
M EQU 2
P EQU 2
;A DB 4, -3, 5, 1, 3, -5, 0, 11, -5, 12, 4, -5; row form
;B DB -2, 5, 4, 9, 3, -1, 3, -7 ;  column form
A DB 1, 0, 0, 1
B DB 1, 0, 0, 1
;A DB N*M DUP(?) 
;B DB P*M DUP(?)
C DW N*P DUP(0)
.CODE   
.STARTUP
            ; matrix multiplication code
            ; no overflow management  
            XOR DI, DI       ; DI = 0, index of C matrix
            XOR SI, SI       ; SI = 0  
            XOR BP, BP
            XOR BX, BX  
            XOR AX, AX
            XOR CX, CX
populating: ; this loop popolate C 
            ; matrix in a row format
            
extLoop:    XOR BP, BP           
intLoop:    XOR SI, SI ; SI manage the row-column product for each i-j of the matrix
evaluate:   MOV AL, A[BX][SI] 
            MOV CL, B[BX][SI]
            IMUL CL
            ADD C[DI], AX  
            INC SI         
            CMP SI, M
            JB evaluate     
            
            ADD BP, 2*M
            CMP BP, 2*M*P
            JB intLoop
            
            ADD BX, 2*M
            CMP BX, 2*N*M
            JB extLoop            
            ADD DI, 2        ; DI increased by two to save
                             ; a word(16bit)
            CMP DI, 2*N*P    ; matrix dimension in byte
            JB populating     
.EXIT
END